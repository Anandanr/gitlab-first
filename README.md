This is to demonstrate use of docker build and Rolling update strategy.

Docker build and push to docker registry:

docker build -t anand7007/mediawiki.1.35 .

docker login

docker push anand7007/mediawiki.1.35

Below command for Rolling update.

First time it will be install
next time it will be update

helm install tom-release ./mediawiki 

helm upgrade --install mediawiki-app --set appName=mediawiki-roll ./mediawiki

mediawiki-app is my release name and passing runtime values to helm chart using --set option